<?php
foreach (array("()", "[(])", "{([]{()})}", "{([]{()})}(", "{([]{()})") as $sample) {
    echo "$sample: " . (parsString($sample) ? "true" : "false"). "<br/>";
}

/**
 * The method checks the order of the brackets in the line.
 * @param $string string to check.
 * @return bool returns true when the order in brackets is correct and false when the order is incorrect.
 */
function parsString(string $string): bool
{
    if (checkString($string)) return false;
    $count = strlen($string) / 2;

    $string = replaceBrackets($string, $count);

    if (strlen($string) == 0) {
        return true;
    }
    return false;
}

/**
 * The method checks the string for foreign characters in the string. The line length is also checked.
 * @param $string string to check.
 * @return bool the method returns true if the string does not match the validation parameters
 */
function checkString(string $string): bool
{
    if (strlen($string) % 2 != 0) {
        return true;
    }

    if (preg_match_all('/[^\\\\(\\\\)\\\\{\\\\}\\[\\]]/', preg_quote($string))) {
        return true;
    }
    return false;
}

/**
 * The method checks the string character by character. When you find pairs of brackets, he removes them
 * from the line.
 * @param $string string in which is searching the pairs of brackets.
 * @param int $count counter to exit recursion.
 * @return string line after deleting the found pairs of brackets.
 */
function replaceBrackets(string $string, int $count): string
{
    for ($i = 0; $i < (strlen($string)-1); $i++) {
        if (ord($string[$i]) === ((ord($string[$i + 1]) - 1)) || ord($string[$i]) === ((ord($string[$i + 1])) - 2)) {
            $string = substr_replace($string, '', $i, 2);
        }
    }

    $count--;

    if (strlen($string) > 1 & $count != 0) {
        $string = replaceBrackets($string, $count);
    }
    return $string;
}


